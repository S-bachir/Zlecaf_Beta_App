

class Pays:

    def __init__(self, nom_pays, PIB_pays, population_pays, taux_demographique_pays, taux_croissance_pays, inflation_pays, part_exportation_intra_pays, exportation_intra_pays):
        self.nom_pays = nom_pays
        self.PIB_pays = PIB_pays
        self.population_pays = population_pays
        self.taux_demographique_pays = taux_demographique_pays
        self.taux_croissance_pays = taux_croissance_pays
        self.inflation_pays = inflation_pays
        self.part_exportation_intra_pays = part_exportation_intra_pays
        self.exportation_intra_pays = exportation_intra_pays
